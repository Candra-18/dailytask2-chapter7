import React from "react";
import { Menu } from "antd";

function AppHeader() {
  return (
    <div className="container-fluid">
      <div className="header">
        <div className="logo">
          <a href="http://www.google.com"></a>
        </div>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["Home"]}>
          <Menu.Item key="Home">Home</Menu.Item>
          <Menu.Item key="About">About</Menu.Item>
          <Menu.Item key="features">features</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
      </div>
    </div>
  );
}
export default AppHeader;
