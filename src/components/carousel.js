import React from "react";
import "antd/dist/antd.css";
import "../index";
import { Carousel } from "antd";

const contentStyle = {
  height: "520px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
const imagetStyle = {
  height: "520px",
  width: "100%",
  textAlign: "center",
};

export default () => (
  <Carousel autoplay>
    <div>
      <h3 style={contentStyle}>
        <img style={imagetStyle} src="https://snipstock.com/assets/cdn/png/3829fe74f647359a15e3227783652323.png" alt="" />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img style={imagetStyle} src="https://snipstock.com/assets/cdn/png/3829fe74f647359a15e3227783652323.png" alt="" />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img style={imagetStyle} src="https://snipstock.com/assets/cdn/png/3829fe74f647359a15e3227783652323.png" alt="" />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img style={imagetStyle} src="https://snipstock.com/assets/cdn/png/3829fe74f647359a15e3227783652323.png" alt="" />
      </h3>
    </div>
  </Carousel>
);
