import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./Module";
import Styled from "./Styled";
import "./index.css";
import "antd/dist/antd.css";

// import componen files
import AppHeader from "./components/header";
import Carousel from "./components/carousel";
import Tabel from "./components/Tabel";


import { Button, DatePicker } from "antd";
import { Layout } from "antd";
import Card from "./components/card";



const { Header } = Layout;
const LayoutStyle = {
marginTop : "50px",
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Layout className="mainLayout" >
      <Header>
        <AppHeader />
      </Header>
    </Layout> 

    <Carousel style={LayoutStyle}/>
    <div style={LayoutStyle}>
    <Card />
    </div>

    <div style={LayoutStyle}>
    <h1 className="fw-bold">Table Penjualan</h1>
    <Tabel />
    </div>

    <div>
      <DatePicker />
    </div>

    <main>
      <h1>Hello World</h1>
    </main>

    <Module />

    <Styled />
  </React.StrictMode>
);
